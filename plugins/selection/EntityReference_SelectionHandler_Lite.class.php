<?php

/**
 * @file
 * Contains EntityReference_SelectionHandler_Lite.
 */

/**
 * A lightweight Entity handler.
 *
 * The generic base implementation has a variety of overrides to workaround
 * core's largely deficient entity handling.
 */
class EntityReference_SelectionHandler_Lite extends EntityReference_SelectionHandler_Generic {

  /**
   * {@inheritdoc}
   */
  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    $target_entity_type = $field['settings']['target_type'];

    // Check if the entity type does exist and has a base table.
    $entity_info = entity_get_info($target_entity_type);
    if (empty($entity_info['base table'])) {
      return EntityReference_SelectionHandler_Broken::getInstance($field, $instance);
    }

    return new EntityReference_SelectionHandler_Lite($field, $instance, $entity_type, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getReferencableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $options = array();
    $entity_type = $this->field['settings']['target_type'];
    $bundles = !empty($this->field['settings']['handler_settings']['target_bundles'])
      ? $this->field['settings']['handler_settings']['target_bundles']
      : array();

    $entity_info = entity_get_info($entity_type);

    // Figure out table and keys. This is necessary because EFQ is great and all
    // but we really just want a quick & dirty query to ged ids and labels from
    // the database without loading every single entity object. This looks like
    // it might be possible with EFQ using https://www.drupal.org/project/efq_extra_field
    // but let's just keep it simple and use DBTNG instead.
    $table = $entity_info['base table'];
    $id_key = $entity_info['entity keys']['id'];
    $label_key = $entity_info['entity keys']['label'];
    $bundle_key = $entity_info['entity keys']['bundle'];

    $query = db_select($table, 't')
      ->fields('t', array($id_key, $label_key, $bundle_key));

    // Minor optimization if we only have 1 bundle (most common use-case).
    if (count($bundles) === 1) {
      $query->condition("t.{$bundle_key}", reset($bundles), '=');
    }
    else {
      $query->condition("t.{$bundle_key}", $bundles, 'IN');
    }

    if ($limit > 0) {
      $query->range(0, $limit);
    }

    $results = $query->execute();

    foreach ($results->fetchAll() as $result) {
      $bundle = $result->{$bundle_key};
      $id = $result->{$id_key};
      $label = $result->{$label_key};
      $options[$bundle][$id] = check_plain($label);
    }

    return $options;
  }

  /**
   * Implements EntityReferenceHandler::getLabel().
   */
  public function getLabel($entity) {
    include_once drupal_get_path('module', 'devel') . '/krumo/class.krumo.php';
    krumo($entity);die;
  }
}
